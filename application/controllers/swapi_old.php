<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class swapi_old extends CI_Controller {

    private $cachePath = 'application/cache/swapi/';
    private $cacheTime = 86400; // 24 hours in seconds
    // private $cacheTime = 600; // 10  seconds

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
     
        
        $this->load->driver('cache', array('adapter' => 'file'));
    }

 
 
    function index()
	{    
		$this->data['data'] = "";
        // $this->data['combined_data'] = $this->fetchAllPeopleFromSWAPI();
		$this->data['isi'] = $this->load->view('search_fromV', $this->data, TRUE);
		$this->load->view('layout', $this->data);
	}

    function searchPeople()
    {
        $url = "https://swapi.dev/api/people/?page=1";
        $combined_results = array();
        
        // Lakukan looping sampai tidak ada lagi halaman selanjutnya
        while ($url) {
            // Ambil data dari URL saat ini
            $response = file_get_contents($url);
            $result = json_decode($response, true);
        
            // Gabungkan hasil dengan array yang sudah ada
            $combined_results = array_merge($combined_results, $result['results']);
        
            // Periksa apakah ada halaman berikutnya
            $url = $result['next'];
        }
        
        // Buat array baru yang akan menjadi hasil gabungan
        $combined_data = [
            'count' => count($combined_results), // Jumlah total data
            'results' => $combined_results // Hasil gabungan dari semua halaman
        ];
        
        // Konversi array menjadi JSON
        echo json_encode($combined_data);        
        // echo json_encode($this->M_kategori->get_employee($q));
    }

    public function search() {
        $name = $this->input->post('name');
        $cacheFile = $this->cachePath . md5($name) . '.json';
        if (file_exists($cacheFile) && (time() - filemtime($cacheFile)) < $this->cacheTime) {
            $result = json_decode(($cacheFile), true);
        } else {
            $url = "https://swapi.dev/api/people/?search=" . urlencode($name);
            $response = file_get_contents($url);
            $result = json_decode($response, true);

            if (!empty($result['results'])) {
                write_file($cacheFile, json_encode($result));
            }
        }

        $data['results'] = $result['results'] ?? [];
        $this->load->view('search_results', $data);
    }

    private function callSwapi($endpoint) {
        $cacheKey = md5($endpoint);
        $cachedData = $this->cache->get($cacheKey);

        if ($cachedData) {
            return $cachedData;
        } else {
            $response = file_get_contents($endpoint);
            $data = json_decode($response, true);
            // Cache for 1 day
            $this->cache->save($cacheKey, $data, 86400);
            return $data;
        }
    }

    public function searchPerson() {
        $name = $this->input->post('name');
        $data = $this->callSwapi("https://swapi.dev/api/people/?search=" . urlencode($name));

        $result = [];
        if (!empty($data['results'])) {
            foreach ($data['results'] as $person) {
                $homeworldResponse = $this->callSwapi($person['homeworld']);
                $starships = [];
                foreach ($person['starships'] as $starshipUrl) {
                    $starshipData = $this->callSwapi($starshipUrl);
                    $starships[] = [
                        'name' => $starshipData['name'],
                        'model' => $starshipData['model']
                    ];
                }
                $result[] = [
                    'name' => $person['name'],
                    'gender' => $person['gender'],
                    'homeworld' => $person['homeworld'],
                    'homeworld_name' => $homeworldResponse['name'],
                    'starships' => $starships
                ];
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function planet($id) {
        $data = $this->callSwapi("https://swapi.dev/api/planets/{$id}/");
        $result = [
            'name' => $data['name'],
            'population' => $data['population']
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}
