<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class swapi_M extends CI_Model {

    protected $cachePath = 'application/cache/';
    protected $cacheTime = 86400; // 1 day

    public function getPersonByName($name)
    {
        $cacheFile = $this->cachePath . md5($name) . '.json';
        if (file_exists($cacheFile) && (filemtime($cacheFile) + $this->cacheTime > time())) {
            $result = json_decode(file_get_contents($cacheFile), true);
        } else {
            $url = "https://swapi.dev/api/people/?search=" . urlencode($name);
            $response = file_get_contents($url);
            $result = json_decode($response, true)['results'][0] ?? null;
            if ($result) {
                $this->cacheData($cacheFile, $result);
            }
        }
        return $this->filterPersonData($result);
    }

    protected function cacheData($file, $data)
    {
        file_put_contents($file, json_encode($data));
    }

    protected function filterPersonData($data)
    {
        if (!$data) return null;
        // Filter data here as per requirements
        return [
            'name' => $data['name'],
            'gender' => $data['gender'],
            // Add more fields as needed
        ];
    }
}
